name = "Jian"
age = 22
occupation = "student"
movie = "Morbius"
rating = 69.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating} %")

num1 = 10
num2 = 20
num3 = 30

print(num1 * num2)
print(num1 < num3)

num2 += num3