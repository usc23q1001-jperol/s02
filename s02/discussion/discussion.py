# Comments
print("Hello World")

# Variables
age = 18
middle_initial = "C"
name1, name2, name3, name4 = "john", "paul", "george", "ringo"

# Data Types
# Strings (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# Numbers (int, float, complex)
num_of_days = 365
pi_approx = 3.1416
complex_numb = 1 + 5j

# Boolean 
isLearning = True
isDifficult = False

# Using Variables
print("My name is " + full_name)
print("My age is " + str(age))

# Typecasting
# int() - converts the value to integer value
# float() - converts the value to a float value
# 

print(int(3.5))
print(int("9861"))

# F-strings, add a lowercase "f" before the string and place variable in {}
print(f"My age is {age}")
print(f"Hi, my name is {full_name} and my age is {age}")

# Operations
print(1 + 10)